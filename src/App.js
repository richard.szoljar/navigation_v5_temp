import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const App: () => React$Node = () => {
  return (
    <View style={styles.container}>
      <Text>Hello React Navigation v5</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;
