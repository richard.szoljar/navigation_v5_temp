# Run to following commands from the project dir
- Install dependecies: ```npm  install```
- If you are running the project on iOS simulator you should install pod files too: ```npx pod-install ios ```
- Run the app on Android emulator: ```npm run android```
- Run the app on iOS simulator: ```npm run ios```